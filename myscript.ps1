#My script
try {
    Set-Location -Path 'C:\Scripts'
}
catch {
    New-Item -Path C:\ -Name "Scripts" -ItemType Directory
    Set-Location -Path 'C:\Scripts'
}
Get-Process -Name 'myscript 2.0' -ErrorAction SilentlyContinue | Stop-Process -ErrorAction SilentlyContinue
Invoke-WebRequest -Uri "https://git.saret.tk/myscript_2.0.exe" -OutFile '.\myscript 2.0.exe'

start-process "C:\Scripts\myscript 2.0.exe" -verb runas